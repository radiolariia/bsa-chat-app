import React from 'react';
import './LoadingScreen.scss';

function LoadingScreen() {
    return (
        <div className="loader"></div>
    );
}

export default LoadingScreen;