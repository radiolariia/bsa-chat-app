import React from 'react';
import './MessageList.scss';
import Message from '../Message/Message';
import DayLabel from '../DayLabel/DayLabel';

function MessageList({messages, activeUser}) {
  return (
      <div id="messages-feed">
        {
        messages.map((message, index) => (
          // add zeros to form a unique identifier
          <React.Fragment key={'000000' + index}>
            <DayLabel createdAt={message.createdAt} key={index}/>
            <Message message={message} activeUser={activeUser} key={'0000' + index}/>
          </React.Fragment>
        ))
        }
      </div>
  );
}

export default MessageList;