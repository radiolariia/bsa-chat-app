import React, { useState, useEffect } from 'react';
import { messageService } from '../../services/messageService';
import './Chat.scss';
import LoadingScreen from '../LoadingScreen/LoadingScreen.jsx';
import Header from '../Header/Header';
// eslint-disable-next-line
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';
import { v4 } from 'uuid';

function Chat() {
    const [isLoading, setIsLoading] = useState(true);
    const [messages, setMessages] = useState([]);
    // eslint-disable-next-line
    const [activeUser, setActiveUser] = useState('Ruth');
    // eslint-disable-next-line
    const [activeUserAvatar, setActiveUserAvatar] = useState('https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA');
    
    useEffect(() => {
      async function loadData () {
        const messages = await messageService.getAllMessages(); 
        
        const sortedMessagesList = messages.sort((a, b) => {
          return b.createdAt - a.createdAt;
        })

      setMessages(sortedMessagesList);
      setIsLoading(false);

      async function fetchData (url) {
        let result = await fetch(url)
        .then(response => {
          console.log(response.ok)
          if(response.ok) {
            return response.json();
          } else {
            throw new Error('Failed to load');
          }
        })
        .catch(error => {
            throw error;
        });
        
        return result;
      }
      }
      
      loadData();

    }, []);
    
    const sendMessage = (text) => {
      console.log('works')
      const newMessage = { 
        // v4 is a uuid generator
        "id": v4(), 
        "text": text, 
        "user": activeUser, 
        "avatar": activeUserAvatar, 
        "editedAt": "", 
        "createdAt": new Date()
      };

      setMessages([...messages, newMessage]);
    }

    const participantsSet = new Set(messages.map(message => message.user));
  return (
      <React.Fragment>
          {isLoading ? <LoadingScreen/> : null}
          <img id="logo" src={require("../../assets/logo.png")} alt="paper plane logo"/>
          <div className="main-content">
            <Header
              chatName="My chat"
              numOfParticipants={participantsSet.size}
              numOfMessages={messages.length}
              lastMessageTime={14.28}
            />
            <MessageList messages={messages} activeUser={activeUser}/>
            <MessageInput sendMessage={sendMessage}/>
          </div>
      </React.Fragment>
  );
}

export default Chat;