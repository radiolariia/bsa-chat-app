export async function callApi (url) {
    let result = await fetch(url)
    .then(response => {
      if(response.ok) {
        return response.json();
      } else {
        throw new Error('Failed to load');
      }
    })
    .catch(error => {
        throw error;
    });
    
    return result;
}